﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace MagentoModuleGenerator.Classes
{
	[Serializable]
	public class ClassConfig
	{
		/// <summary>
		/// Назва класу. Перша буква у верхньому регістрі, а всі інші у нижньому
		/// </summary>
		public string Name { get; set; }
		/// <summary>
		/// Назва таблиці для класу
		/// </summary>
		public string Table { get; set; }
		/// <summary>
		/// Заголовок класу
		/// </summary>
		public string Title { get; set; }
		/// <summary>
		/// Заголовок класу у множині
		/// </summary>
		public string TitlePlural { get; set; }
		/// <summary>
		/// Поля класу
		/// </summary>
		public List<FieldConfig> Fields { get; set; }

		public override string ToString()
		{
			return Name;
		}

		/// <summary>
		/// Заголовок класу у множині
		/// Приготовано для виводу
		/// </summary>
		[XmlIgnore]
		public string TitlePluralOut
		{
			get
			{
				return string.IsNullOrWhiteSpace(TitlePlural) ? Title : TitlePlural;
			}
		}

		/// <summary>
		/// Назва контроллеру для адмінки.
		/// </summary>
		[XmlIgnore]
		public string ControllerNameAdminhtml
		{
			get
			{
				return Name;
			}
		}

		/// <summary>
		/// Назва контроллеру для frontend
		/// </summary>
		[XmlIgnore]
		public string ControllerNameFrontend
		{
			get
			{
				return Name;
			}
		}

		/// <summary>
		/// ID поле
		/// </summary>
		[XmlIgnore]
		public FieldConfig PrimaryField
		{
			get
			{
				if (Fields != null)
				{
					foreach (FieldConfig field in Fields)
					{
						if (field.Primary)
							return field;
					}
				}
				throw new Exception(string.Format("У класі %s не знайдено ID-поля", Name));
			}
		}

		/// <summary>
		/// Поле назви для класу
		/// </summary>
		[XmlIgnore]
		public FieldConfig NameField
		{
			get
			{
				if (Fields != null)
				{
					foreach (FieldConfig field in Fields)
					{
						if (field.IsName)
							return field;
					}
				}
				return null;
			}
		}
		/// <summary>
		/// Чи є серед полів тке яке редагується HTML-редактором
		/// Потрібно для генерації відповідного layout-update щоб підключити редактор
		/// </summary>
		[XmlIgnore]
		public bool HasHtmlEditor
		{
			get
			{
				if (Fields != null)
				{
					foreach (FieldConfig field in Fields)
					{
						if (field.IsHtml)
							return true;
					}
				}
				return false;
			}
		}

		public void Generate(GenerateConfig config, DirectoryInfo path)
		{
			DirectoryInfo pathCode = path.CreateSubdirectory("app\\code\\local\\" + config.Namespace + "\\" + config.Module);
			DirectoryInfo pathEtc = pathCode.CreateSubdirectory("etc");
			DirectoryInfo pathControllers = pathCode.CreateSubdirectory("controllers");
			DirectoryInfo pathControllersAdmin = pathControllers.CreateSubdirectory("Adminhtml");
			DirectoryInfo pathHelper = pathCode.CreateSubdirectory("Helper");
			DirectoryInfo pathModel = pathCode.CreateSubdirectory("Model");
			pathModel.CreateSubdirectory("Resource\\" + Name);
			//DirectoryInfo pathInstall = pathCode.CreateSubdirectory("sql\\" + config.Module.ToLower() + "_setup");
			DirectoryInfo pathBlockAdmin = pathCode.CreateSubdirectory("Block\\Adminhtml\\"+config.Module.CapitalizeFirst());
			pathBlockAdmin.CreateSubdirectory(this.Name+"\\Edit\\Tab");
			//pathModel.CreateSubdirectory("Resource\\" + Name);
			
			#region Клас, колекція та ресурси
			string res = new MagentoModuleGenerator.Templates.app.code.local.Namespace.Module.Model.Class_php(config, this).TransformText();
			res.SaveToFile(Path.Combine(pathModel.FullName, Name + ".php"));
			res = new MagentoModuleGenerator.Templates.app.code.local.Namespace.Module.Model.Resource.Class_php(config, this).TransformText();
			res.SaveToFile(Path.Combine(pathModel.FullName, "Resource\\" + Name + ".php"));
			res = new MagentoModuleGenerator.Templates.app.code.local.Namespace.Module.Model.Resource.Setup_php(config, this).TransformText();
			res.SaveToFile(Path.Combine(pathModel.FullName, "Resource\\Setup.php"));
			res = new MagentoModuleGenerator.Templates.app.code.local.Namespace.Module.Model.Resource.Class.Collection_php(config, this).TransformText();
			res.SaveToFile(Path.Combine(pathModel.FullName, "Resource\\" + Name + "\\Collection.php"));
			#endregion

			#region Блоки

			if (config.GenerateFrontend)
			{
				#region frontend
				DirectoryInfo pathBlock = pathCode.CreateSubdirectory("Block\\" + this.Name);
				res = new MagentoModuleGenerator.Templates.app.code.local.Namespace.Module.Block.Class.Grid_php(config, this).TransformText();
				res.SaveToFile(Path.Combine(pathBlock.FullName, "Grid.php"));
				res = new MagentoModuleGenerator.Templates.app.code.local.Namespace.Module.Block.Class.View_php(config, this).TransformText();
				res.SaveToFile(Path.Combine(pathBlock.FullName, "View.php"));

				#region Templates
				DirectoryInfo pathTemplates = path.CreateSubdirectory("app\\design\\frontend\\" + config.FrontendDesignOut + "\\template\\" + config.FullName.ToLower() + "\\" + this.Name.ToLower());
				res = new MagentoModuleGenerator.Templates.app.design.frontend.default1.default1.template.namespace_module.class1.grid_phtml(config, this).TransformText();
				res.SaveToFile(Path.Combine(pathTemplates.FullName, "grid.phtml"));
				res = new MagentoModuleGenerator.Templates.app.design.frontend.default1.default1.template.namespace_module.class1.view_phtml(config, this).TransformText();
				res.SaveToFile(Path.Combine(pathTemplates.FullName, "view.phtml"));
				#endregion

				#endregion
			}

			#region Adminhtml
			res = new MagentoModuleGenerator.Templates.app.code.local.Namespace.Module.Block.Adminhtml.Module.Class_php(config, this).TransformText();
			res.SaveToFile(Path.Combine(pathBlockAdmin.FullName, this.Name + ".php"));
			res = new MagentoModuleGenerator.Templates.app.code.local.Namespace.Module.Block.Adminhtml.Module.Class.Edit_php(config, this).TransformText();
			res.SaveToFile(Path.Combine(pathBlockAdmin.FullName, this.Name + "\\Edit.php"));
			res = new MagentoModuleGenerator.Templates.app.code.local.Namespace.Module.Block.Adminhtml.Module.Class.Grid_php(config, this).TransformText();
			res.SaveToFile(Path.Combine(pathBlockAdmin.FullName, this.Name + "\\Grid.php"));
			res = new MagentoModuleGenerator.Templates.app.code.local.Namespace.Module.Block.Adminhtml.Module.Class.Edit.Form_php(config, this).TransformText();
			res.SaveToFile(Path.Combine(pathBlockAdmin.FullName, this.Name + "\\Edit\\Form.php"));
			res = new MagentoModuleGenerator.Templates.app.code.local.Namespace.Module.Block.Adminhtml.Module.Class.Edit.Tabs_php(config, this).TransformText();
			res.SaveToFile(Path.Combine(pathBlockAdmin.FullName, this.Name + "\\Edit\\Tabs.php"));
			res = new MagentoModuleGenerator.Templates.app.code.local.Namespace.Module.Block.Adminhtml.Module.Class.Edit.Tab.Form_php(config, this).TransformText();
			res.SaveToFile(Path.Combine(pathBlockAdmin.FullName, this.Name + "\\Edit\\Tab\\Form.php"));
			#endregion

			#endregion

			#region Контролери
			// frontend
			res = new MagentoModuleGenerator.Templates.app.code.local.Namespace.Module.controllers.ClassController_php(config, this).TransformText();
			res.SaveToFile(Path.Combine(pathControllers.FullName, this.Name + "Controller.php"));
			// backend
			res = new MagentoModuleGenerator.Templates.app.code.local.Namespace.Module.controllers.Adminhtml.ClassController_php(config, this).TransformText();
			res.SaveToFile(Path.Combine(pathControllersAdmin.FullName, this.Name + "Controller.php"));
			#endregion
		}


	}
}
