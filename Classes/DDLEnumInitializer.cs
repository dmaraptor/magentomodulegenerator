using System;
using System.ComponentModel;
using DevExpress.XtraEditors.DXErrorProvider;
using Tsg.Entity.Common.Attributes;
using DevExpress.XtraEditors;
using System.Drawing;
using System.Windows.Forms;
using System.Collections;
using Tsg.Entity.Common;

namespace Tsg.CCL
{
	public class DDLEnumInitializer
	{
		/// <summary>
		/// �������� ���������� ������ ���������� � ����� � ��������� ����
		/// </summary>
		/// <param name="edit">KookUpEdit</param>
		/// <param name="EnumType">����</param>
		/// <param name="NullText">����� ���� ������������� ���� �������� null</param>
		public static void SetEnumDDL(DevExpress.XtraEditors.LookUpEdit edit, Type EnumType, string NullText)
		{
			SetEnumDDL((DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit)edit.Properties, EnumType, NullText, null);
		}

		/// <summary>
		/// �������� ���������� ������ ���������� � ����� � ��������� ����
		/// </summary>
		/// <param name="edit">KookUpEdit</param>
		/// <param name="EnumType">����</param>
		public static void SetEnumDDL(DevExpress.XtraEditors.LookUpEdit edit, Type EnumType)
		{
			SetEnumDDL((DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit)edit.Properties, EnumType, "", null);
		}

		/// <summary>
		/// �������� ���������� ������ ���������� � ����� � ��������� ����
		/// </summary>
		/// <param name="edit">KookUpEdit</param>
		/// <param name="EnumType">����</param>
		/// <param name="exceptValues">��������� � ������ �������� �������� �����</param>
		public static void SetEnumDDL(DevExpress.XtraEditors.LookUpEdit edit, Type EnumType, object[] exceptValues)
		{
			SetEnumDDL((DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit)edit.Properties, EnumType, "", exceptValues);
		}

		/// <summary>
		/// �������� ���������� ������ ���������� � ����� � ��������� ����
		/// </summary>
		/// <param name="edit">RepositoryItemLookUpEdit</param>
		/// <param name="EnumType">����</param>
		/// <param name="exceptValues">��������� � ������ �������� �������� �����</param>
		public static void SetEnumDDL(DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit edit, Type EnumType, object[] exceptValues)
		{
			SetEnumDDL(edit, EnumType, "", exceptValues);
		}

		/// <summary>
		/// �������� ���������� ������ ���������� � ����� � ��������� ����
		/// </summary>
		/// <param name="rep_edit">RepositoryLookUpEdit</param>
		/// <param name="EnumType">����</param>
		public static void SetEnumDDL(DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit rep_edit, Type EnumType)
		{
			SetEnumDDL(rep_edit, EnumType, "", null);
		}

		/// <summary>
		/// �������� ���������� ������ ���������� � ����� � ��������� ����
		/// </summary>
		/// <param name="rep_edit">RepositoryLookUpEdit</param>
		/// <param name="EnumType">����</param>
		/// <param name="NullText">����� ���� ������������� ���� �������� null</param>
		public static void SetEnumDDL(DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit rep_edit, Type EnumType, string NullText)
		{
			SetEnumDDL(rep_edit, EnumType, NullText, null);
		}

		/// <summary>
		/// �������� ���������� ������ ���������� � ����� � ��������� ����
		/// </summary>
		/// <param name="rep_edit">RepositoryLookUpEdit</param>
		/// <param name="EnumType">����</param>
		/// <param name="NullText">����� ���� ������������� ���� �������� null</param>
		/// <param name="exceptValues">��������� � ������ �������� �������� �����</param>
		public static void SetEnumDDL(DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit rep_edit, Type EnumType, string NullText, object[] exceptValues)
		{
			SetEnumDDL(rep_edit, LookUpItem.GetEnumValues(EnumType), NullText, exceptValues);
		}

		/// <summary>
		/// �������� ���������� ������ ���������� � ����� � ��������� ����
		/// </summary>
		/// <param name="edit">LookUpEdit</param>
		/// <param name="EnumValues">������ ������� ����� ���� ���� �������� � ������</param>
		public static void SetEnumDDL(DevExpress.XtraEditors.LookUpEdit edit, ArrayList EnumValues)
		{
			SetEnumDDL((DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit)edit.Properties, EnumValues, "", null);
		}

		/// <summary>
		/// �������� ���������� ������ ���������� � ����� � ��������� ����
		/// </summary>
		/// <param name="rep_edit">RepositoryLookUpEdit</param>
		/// <param name="EnumValues">������ ������� ����� ���� ���� �������� � ������</param>
		/// <param name="NullText">����� ���� ������������� ���� �������� null</param>
		/// <param name="exceptValues">��������� � ������ �������� �������� �����</param>
		public static void SetEnumDDL(DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit rep_edit, ArrayList EnumValues, string NullText, object[] exceptValues)
		{
			ArrayList ar1 = EnumValues;

			//���� � �������� ����� �� ������� ����� � ������
			if (null != exceptValues)
			{
				foreach (LookUpItem item in new ArrayList(ar1))
				{
					bool exist = false;
					foreach (object tmp in exceptValues)
					{
						if ((int)tmp == (int)item.Val)
						{
							exist = true;
							break;
						}
					}
					if (exist)
					{
						ar1.Remove(item);
					}
				}
			}
			rep_edit.DataSource = ar1;
			rep_edit.DropDownRows = ar1.Count;
			rep_edit.DisplayMember = "Name";
			rep_edit.ValueMember = "Val";
			rep_edit.ShowHeader = false;
			rep_edit.NullText = NullText;
			if (rep_edit.Columns.Count == 0)
				rep_edit.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] { new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name") });
		}
	}
}
