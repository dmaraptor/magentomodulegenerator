using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;
using System.ComponentModel;
using System.Text.RegularExpressions;
using System.Reflection;

namespace Tsg.Entity.Common.Attributes
{
	[AttributeUsage(AttributeTargets.All, Inherited = true, AllowMultiple = false)]
	public class EnumAttribute : Attribute
	{
		string[] captions = null;
		public EnumAttribute(params string[] captions)
		{
			this.captions = captions;
		}
		public string[] Captions
		{
			get
			{
				return captions;
			}
		}
		/// <summary>
		/// ������� �������� Enum � ������� �� ��������� � �� ��������� �������� (�� Enum.GetValues)
		/// </summary>
		/// <param name="enumType">��� Enum</param>
		/// <returns></returns>
		public static Array GetValues(Type enumType)
		{
			Array resultValues = Array.CreateInstance(enumType, Enum.GetValues(enumType).Length);
			int currentIndex = 0;
			foreach (FieldInfo enumField in enumType.GetFields())
			{
				if (!enumField.IsSpecialName)
				{
					resultValues.SetValue(enumField.GetValue(null),currentIndex++);
				}
			}
			return resultValues;
		}
		public static string GetNameByValue(object val)
		{
			return GetNameByValue(val.GetType(), val);
		}
		/// <summary>
		/// ������� "�������" ��� ��� enum ��������
		/// </summary>
		/// <param name="enumType">��� enum</param>
		/// <param name="val">�������� enum</param>
		/// <returns>������</returns>
		public static string GetNameByValue(Type enumType, object val)
		{
			string result = val.ToString();
			EnumAttribute attr = TypeDescriptor.GetAttributes(enumType)[typeof(EnumAttribute)] as EnumAttribute;
			//���� � ��������� �������
			//�� ������ �������� � �������
			if (null != attr)
			{
				Array values = EnumAttribute.GetValues(enumType);
				for (int i = 0; i < values.Length; i++)
				{
					if (Convert.ChangeType(val, enumType).Equals(values.GetValue(i)) && attr.Captions.Length >= i)
						result = attr.Captions[i];
				}

			}
			else
			{
				//������� ������� Description ��� ��������� enum-�
				string enumName = Enum.GetName(enumType, val);
				if (enumName != null)
				{
					FieldInfo info = enumType.GetField(enumName);
					if (null != info)
					{
						object[] attributes = info.GetCustomAttributes(typeof(DescriptionAttribute), false);
						if (null != attributes && attributes.Length > 0)
						{
							DescriptionAttribute desciptionAttribute = attributes[0] as DescriptionAttribute;
							result = desciptionAttribute.Description;
						}
					}
				}
			}
			return result;
		}
	}
	public class TAttributeParser
	{
		private string FAttrStr = "";
		private string FAttrName;
		private ArrayList FParams;

		public TAttributeParser()
		{
			FParams = new ArrayList();
		}

		public static TAttributeParser Parse(string AttributeString)
		{
			TAttributeParser ap = new TAttributeParser();
			ap.FAttrStr = AttributeString;
			ap.Parse();
			return ap;
		}

		public void Parse()
		{
			//[Column("Property1",VisibleIndex=0,Width=100,MinWidth=50,ColumnColor=0,Editor=EditorType.etEdit,Selector=SelectorType.stTraliVali,IsFixed=false, IsFiltered=true)]
			FParams.Clear();
			FAttrName = FAttrStr.Replace("[", "");
			FAttrName = Regex.Replace(FAttrName, "\\(.*?\\)\\]", "", RegexOptions.IgnoreCase | RegexOptions.Singleline).Trim("\r\n\t ".ToCharArray());
			string attr_s = Regex.Replace(FAttrStr, "\\[.*?\\(", "", RegexOptions.IgnoreCase | RegexOptions.Singleline);
			attr_s = attr_s.Replace(")]", "").Trim("\r\n\t ".ToCharArray());
			string[] attr_ar = attr_s.Split(',');
			for (int zz = 0; zz < attr_ar.Length; zz++)
			{
				string[] tar1 = attr_ar[zz].Split("=".ToCharArray(), 2);
				//string attr_name = "";
				//string attr_val = "";
				if (tar1.Length >= 1) tar1[0] = tar1[0].Trim("\r\n\t\" ".ToCharArray());
				if (tar1.Length >= 2) tar1[1] = tar1[1].Trim("\r\n\t\" ".ToCharArray());
				FParams.Add(tar1);
			}
		}
		public string AttributeName
		{
			get
			{
				return FAttrName;
			}
		}
		public ArrayList Parameters
		{
			get
			{
				return FParams;
			}
		}

		public static string RemoveSelectorPrefix(string str)
		{
			int pos = str.IndexOf(".");
			if (pos >= 0) return str.Substring(pos + 1, str.Length - pos - 1); else return str;
		}
	}

}
