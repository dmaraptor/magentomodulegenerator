﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MagentoModuleGenerator.Classes
{
	public static class Extensions
	{
		/// <summary>
		/// Переводить першу букву у кожному слові до верхнього регістру
		/// </summary>
		/// <param name="str">Вхідна строка</param>
		/// <returns></returns>
		public static string Capitalize(this string str)
		{
			if (string.IsNullOrWhiteSpace(str))
				return str;
			CultureInfo cultureInfo = Thread.CurrentThread.CurrentCulture;
			TextInfo textInfo = cultureInfo.TextInfo;

			return textInfo.ToTitleCase(str);
		}

		/// <summary>
		/// Переводить першу букву у кожному слові до верхнього регістру
		/// При цьому всі інші букви кожного слова зводяться до нижнього регістру
		/// </summary>
		/// <param name="str">Вхідна строка</param>
		/// <returns></returns>
		public static string CapitalizeFirst(this string str)
		{
			if (string.IsNullOrWhiteSpace(str))
				return str;
			CultureInfo cultureInfo = Thread.CurrentThread.CurrentCulture;
			TextInfo textInfo = cultureInfo.TextInfo;

			return textInfo.ToTitleCase(str.ToLower());
		}

		public static void SaveToFile(this string str, string fileName)
		{
			using (StreamWriter sw = new StreamWriter(fileName, false, new UTF8Encoding(false)))
			{
				sw.Write(str);
			}
		}

		/// <summary>
		/// Переводить строку у вид підходящий для CSV
		/// </summary>
		/// <param name="str">Вхідна строка</param>
		/// <returns>Перетворена строка</returns>
		public static string ToCsv(this string str)
		{
			return (str + "").Replace("\"", "\"\"");
		}
	}
}
