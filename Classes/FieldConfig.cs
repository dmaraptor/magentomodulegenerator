﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MagentoModuleGenerator.Classes
{
    [Serializable]
    public class FieldConfig
    {
        public string Name { get; set; }
				public string Title { get; set; }
        public FieldTypeEnum FieldType { get; set; }
        public int Size { get; set; }
				public int Precision { get; set; }
        public Boolean Unsigned { get; set; }
        public Boolean Nullable { get; set; }
        public Boolean Primary { get; set; }
        public Boolean Identity { get; set; }
        public string Comment { get; set; }
				public Boolean IsUrl { get; set; }
				public Boolean IsHtml { get; set; }
				public Boolean IsFile { get; set; }
				public Boolean IsImage { get; set; }
				public Boolean IsLabel { get; set; }
				public Boolean IsName { get; set; }

        public override string ToString()
        {
            return Name + " (" + FieldType + ")";
        }

				public string FieldEditorType()
				{
					switch (FieldType)
					{
						case FieldTypeEnum.TYPE_BOOLEAN: 
							return "checkbox";
						case FieldTypeEnum.TYPE_DATE:
							return "date";
						case FieldTypeEnum.TYPE_DATETIME:
							return "date";
						case FieldTypeEnum.TYPE_TEXT:
							if (IsFile)
								return "file";
							else if (IsImage)
								return "image";
							else if (IsHtml)
								return "editor";
							else if (Size > 0)
								return "text";
							else 
								return "textarea";
						default:
							return "text";
					}
					return "text";
				}

				public string FieldSize()
				{
					if (FieldType == FieldTypeEnum.TYPE_DECIMAL ||
						FieldType == FieldTypeEnum.TYPE_FLOAT ||
						FieldType == FieldTypeEnum.TYPE_NUMERIC)
						return string.Format("array({0}, {1})", Size, Precision);
					return Size > 0 ? Size + "" : "null";
				}
    }

    public enum FieldTypeEnum
    {
        TYPE_BOOLEAN = 0,
        TYPE_SMALLINT = 1,
        TYPE_INTEGER,
        TYPE_BIGINT,
        TYPE_FLOAT,
        TYPE_NUMERIC,
        TYPE_DECIMAL,
        TYPE_DATE,
        TYPE_TIMESTAMP,
        TYPE_DATETIME,
        TYPE_TEXT,
        TYPE_BLOB,
        TYPE_VARBINARY
    }
}
