﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace MagentoModuleGenerator.Classes
{
	[Serializable]
	public class GenerateConfig
	{
		public string Namespace { get; set; }
		public string Module { get; set; }
		public string ModuleTitle { get; set; }
		public bool GenerateFrontend { get; set; }
		public string FrontendDesign { get; set; }
		public List<ClassConfig> Classes { get; set; }

		[XmlIgnore]
		public string FullName
		{
			get
			{
				return Namespace + "_" + Module;
			}
		}

		[XmlIgnore]
		public string FrontendDesignOut
		{
			get
			{
				return string.IsNullOrWhiteSpace(FrontendDesign) ? "default\\default" : FrontendDesign;
			}
		}

		public static GenerateConfig LoadXml(string fileName)
		{
			GenerateConfig config = null;
			if (File.Exists(fileName))
			{
				System.Xml.Serialization.XmlSerializer Serializer =
						new System.Xml.Serialization.XmlSerializer(typeof(GenerateConfig));

				using (FileStream stream = File.OpenRead(fileName))
				{
					System.Xml.XmlTextReader Reader = new System.Xml.XmlTextReader(stream);
					config = (GenerateConfig)Serializer.Deserialize(Reader);
					Reader.Close();
				}
			}
			return config;
		}

		/// <summary>
		/// Зберігає конфігурацію у файл заданий параметром fileName
		/// </summary>
		/// <param name="fileName">Повне ім'я файлу</param>
		public void Save(string fileName)
		{
			System.Xml.Serialization.XmlSerializer Serializer =
													new System.Xml.Serialization.XmlSerializer(typeof(GenerateConfig));
			FileInfo configFile = new FileInfo(fileName);

			if (!configFile.Directory.Exists)
				configFile.Directory.Create();

			using (FileStream stream = configFile.Open(FileMode.Create))
			{
				Serializer.Serialize(stream, this);
			}
		}

		public override string ToString()
		{
			return Namespace + "_" + Module;
		}
	}
}
