using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Specialized;
using Tsg.Entity.Common.Attributes;
//using Tsg.Sys;

namespace Tsg.Entity.Common
{
	/// <summary>
	/// ���� ��� ��������� ������ LookUtEdit-���. ����������� ��� ������� 
	/// ����������� ������� �������� (� ����������� int-�������� ��������� string-��������).
	/// </summary>
	public class LookUpItem
	{
		#region Fields
		private  object _val = 0;
		private  string _name = string.Empty;
		#endregion 

		#region Methods
		public LookUpItem(object v,string n)
		{
			_val = v; _name = n;
		}
		public LookUpItem(object v)
		{
			_val = v;
			_name = LookUpItem.GetName(v);
		}
		public static ArrayList GetEnumValues(Type enumType)
		{
			ArrayList result = new ArrayList();
			Array values = EnumAttribute.GetValues(enumType);
			for (int i = 0; i < values.Length; i++)
			{
				LookUpItem item = null;
				object val = values.GetValue(i);
				item = new LookUpItem(val, EnumAttribute.GetNameByValue(val));
				result.Add(item);
			}
			return result;
		}
		public static string GetName(object val, Type enumType)
		{
			return EnumAttribute.GetNameByValue(enumType, val);
		}

		public static string GetName(object val)
		{
			if (!val.GetType().IsEnum)
				throw new Exception("��'��� val �� ���� ���� Enum");
			return GetName(val, val.GetType());
		}

		public override string ToString()
		{
			return Name;
		}
		public static object ParseEnum(Type enumType, string enumStr,char separator)
		{
			int result = 0;
			if(enumStr.Trim() != string.Empty)
			{
				string [] values = enumStr.Split(separator);
				foreach(string value in values)
				{
					
					object enumValue = null;
					foreach(object enumVal in Enum.GetValues(enumType))
					{
						string tmp = LookUpItem.GetName(enumVal,enumType);
						if(tmp == value.Trim())
						{
							enumValue = enumVal;
							break;
						}
					}
					if(enumValue != null)
					{
						result = result |(int)enumValue;
					}
				}
			}
			return result;
		}
		public static string EnumToStr(Type enumType, object value, char separator)
		{
			string result = string.Empty;
			foreach(object enumVal in Enum.GetValues(enumType))
			{
				if(0 != ((int)((int)value&(int)enumVal)))
				{
					result += LookUpItem.GetName(enumVal,enumType)+separator;	
				}

			}
			if(result != string.Empty)
				result = result.Substring(0,result.Length-1);
			else
			{
				return "...";
			}

			return result;

		}


		#endregion 

		#region Properties
		public object Val
		{
			get
			{
				return _val;
			}
			set
			{
				_val = value;
			}
		}
		public string Name
		{
			get
			{
				return _name;
			}
		}
		public byte ValByteCode
		{
			get
			{
				byte res = 0;
				try
				{
					res = Convert.ToByte(_val);
				}
				catch	{	}
				return res;
			}
		}
		#endregion 
	}
	
}
