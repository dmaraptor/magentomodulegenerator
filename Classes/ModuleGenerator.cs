﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MagentoModuleGenerator.Classes
{
	public class ModuleGenerator
	{
		private GenerateConfig config = null;

		public ModuleGenerator(GenerateConfig config)
		{
			if (config == null)
				throw new ArgumentNullException("config");
			this.config = config;
		}

		public static void GenerateNow(GenerateConfig config, string pathToGenerateTo)
		{
			new ModuleGenerator(config).Generate(pathToGenerateTo);
		}

		public void Generate(string pathToGenerateTo)
		{
			if (string.IsNullOrWhiteSpace(pathToGenerateTo))
				throw new ArgumentNullException("pathToGenerateTo");
			if (!Directory.Exists(pathToGenerateTo))
				Directory.CreateDirectory(pathToGenerateTo);
			//throw new DirectoryNotFoundException(pathToGenerateTo);
			DirectoryInfo path = new DirectoryInfo(pathToGenerateTo);
			DirectoryInfo pathCode = path.CreateSubdirectory("app\\code\\local\\" + config.Namespace + "\\" + config.Module);
			DirectoryInfo pathModule = path.CreateSubdirectory("app\\etc\\modules");
			DirectoryInfo pathLocaleEN = path.CreateSubdirectory("app\\locale\\en_US");
			DirectoryInfo pathLocaleUA = path.CreateSubdirectory("app\\locale\\uk_UA");
			DirectoryInfo pathBlock = pathCode.CreateSubdirectory("Block");
			DirectoryInfo pathEtc = pathCode.CreateSubdirectory("etc");
			DirectoryInfo pathControllers = pathCode.CreateSubdirectory("controllers");
			DirectoryInfo pathHelper = pathCode.CreateSubdirectory("Helper");
			DirectoryInfo pathModel = pathCode.CreateSubdirectory("Model");
			DirectoryInfo pathInstall = pathCode.CreateSubdirectory("sql\\" + config.FullName.ToLower() + "_setup");
			DirectoryInfo pathLayoutUpdateBackend = path.CreateSubdirectory("app\\design\\adminhtml\\default\\default\\layout");
			DirectoryInfo pathRenderer = pathBlock.CreateSubdirectory("Adminhtml\\"+config.Module+"\\Renderer");

			#region Генеруємо

			#region Конфіги
			string res = new MagentoModuleGenerator.Templates.app.etc.modules.Namespace_Module_xml(config).TransformText();
			res.SaveToFile(Path.Combine(pathModule.FullName, config.FullName + ".xml"));
			res = new MagentoModuleGenerator.Templates.app.code.local.Namespace.Module.etc.adminhtml_xml(config).TransformText();
			res.SaveToFile(Path.Combine(pathEtc.FullName, "adminhtml.xml"));
			res = new MagentoModuleGenerator.Templates.app.code.local.Namespace.Module.etc.config_xml(config).TransformText();
			res.SaveToFile(Path.Combine(pathEtc.FullName, "config.xml"));
			#endregion

			#region Helper
			res = new MagentoModuleGenerator.Templates.app.code.local.Namespace.Module.Helper.Data_php(config).TransformText();
			res.SaveToFile(Path.Combine(pathHelper.FullName, "Data.php"));
			#endregion

			#region Переклади
			res = new MagentoModuleGenerator.Templates.app.locale.en_US.Namespace_Module_csv(config).TransformText();
			res.SaveToFile(Path.Combine(pathLocaleEN.FullName, config.FullName + ".csv"));
			/*res = new MagentoModuleGenerator.Templates.app.locale.uk_UA.Namespace_Module_csv(config).TransformText();
			res.SaveToFile(Path.Combine(pathLocaleUA.FullName, config.FullName + ".csv"));*/
			#endregion

			#region Layout-update
			if (this.config.GenerateFrontend)
			{
				// frontend
				DirectoryInfo pathLayoutUpdateFrontend = path.CreateSubdirectory("app\\design\\frontend\\" + this.config.FrontendDesignOut + "\\layout");
				res = new MagentoModuleGenerator.Templates.app.design.frontend.default1.default1.layout.namespace_module_xml(config).TransformText();
				res.SaveToFile(Path.Combine(pathLayoutUpdateFrontend.FullName, config.FullName.ToLower() + ".xml"));
			}
			// backend
			res = new MagentoModuleGenerator.Templates.app.design.adminhtml.default1.default1.layout.namespace_module_xml(config).TransformText();
			res.SaveToFile(Path.Combine(pathLayoutUpdateBackend.FullName, config.FullName.ToLower() + ".xml"));
			#endregion

			#region Рендерери ячейок гріда
			res = new MagentoModuleGenerator.Templates.app.code.local.Namespace.Module.Block.Adminhtml.Module.Renderer.Checkbox_php(config).TransformText();
			res.SaveToFile(Path.Combine(pathRenderer.FullName, "Checkbox.php"));
			res = new MagentoModuleGenerator.Templates.app.code.local.Namespace.Module.Block.Adminhtml.Module.Renderer.Image_php(config).TransformText();
			res.SaveToFile(Path.Combine(pathRenderer.FullName, "Image.php"));
			res = new MagentoModuleGenerator.Templates.app.code.local.Namespace.Module.Block.Adminhtml.Module.Renderer.Url_php(config).TransformText();
			res.SaveToFile(Path.Combine(pathRenderer.FullName, "Url.php"));
			res = new MagentoModuleGenerator.Templates.app.code.local.Namespace.Module.Block.Adminhtml.Module.Renderer.Html_php(config).TransformText();
			res.SaveToFile(Path.Combine(pathRenderer.FullName, "Html.php"));
			#endregion

			if (config.Classes != null)
			{
				#region Класи та блоки
				foreach (ClassConfig clas in config.Classes)
					clas.Generate(config, path);
				#endregion

				#region Інсталяційний скрипт
				StringBuilder sb = new StringBuilder();
				foreach (ClassConfig clas in config.Classes)
				{
					string resInner = res = new MagentoModuleGenerator.Templates.app.code.local.Namespace.Module.sql.module_setup.install_0_1_0_php(config, clas).TransformText();
					sb.Append(resInner);
				}
				res = new MagentoModuleGenerator.Templates.app.code.local.Namespace.Module.sql.module_setup.install_0_1_0_php_wrapper(config, sb.ToString()).TransformText();
				res.SaveToFile(Path.Combine(pathInstall.FullName, "install-0.1.0.php"));
				#endregion
			}

			#endregion



			return;
		}
	}
}
