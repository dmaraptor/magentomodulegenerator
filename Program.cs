﻿using MagentoModuleGenerator.Classes;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MagentoModuleGenerator
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        //[STAThread]
        static void Main(string[] args)
        {
            //Application.EnableVisualStyles();
            //Application.SetCompatibleTextRenderingDefault(false);
            //Application.Run(new MainForm());
            
            /*GenerateConfig conf1 = new GenerateConfig() { Namespace = "Namespace", Module = "Module" };
            conf1.Classes = new List<ClassConfig>();
            ClassConfig clas = new ClassConfig() {Name = "Class"};
            conf1.Classes.Add(clas);
            clas.Fields = new List<FieldConfig>();
            clas.Fields.Add(new FieldConfig() { 
                Name = "ID",
                FieldType = FieldTypeEnum.TYPE_INTEGER,
                Identity = true,
                Unsigned = true,
                Nullable = false,
                Primary = true,
                Size = 0,
                Comment = "Field comment"
            });
            clas.Fields.Add(new FieldConfig()
            {
                Name = "Name",
                FieldType = FieldTypeEnum.TYPE_TEXT,
                Identity = false,
                Unsigned = false,
                Nullable = true,
                Primary = true,
                Size = 150,
                Comment = "Name comment"
            });

            conf1.Save("config.xml");
            return;*/


            string configFileName = "config.xml";
            if (args != null && args.Length > 0)
                configFileName = args[0].Trim().Trim('"');
            string path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "gener");
            if (args != null && args.Length > 1)
                path = args[1].Trim().Trim('"');

            GenerateConfig conf = GenerateConfig.LoadXml(configFileName);
            ModuleGenerator.GenerateNow(conf, path);
        }
    }
}
