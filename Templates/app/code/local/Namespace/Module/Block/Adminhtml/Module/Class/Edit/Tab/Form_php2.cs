﻿using MagentoModuleGenerator.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MagentoModuleGenerator.Templates.app.code.local.Namespace.Module.Block.Adminhtml.Module.Class.Edit.Tab
{
	public partial class Form_php
	{
		public Form_php(GenerateConfig config, ClassConfig clas)
		{
			this.config = config;
			this.clas = clas;
		}

		public GenerateConfig config { get; set; }
		public ClassConfig clas { get; set; }
	}
}
