﻿using MagentoModuleGenerator.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MagentoModuleGenerator.Templates.app.code.local.Namespace.Module.Block.Adminhtml.Module.Renderer
{
	public partial class Url_php
	{
		public Url_php(GenerateConfig config)
		{
			this.config = config;
		}

		public GenerateConfig config { get; set; }
	}
}
