﻿using MagentoModuleGenerator.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MagentoModuleGenerator.Templates.app.code.local.Namespace.Module.Model.Resource.Class
{
	public partial class Collection_php
	{
		public Collection_php(GenerateConfig config, ClassConfig clas)
		{
			this.config = config;
			this.clas = clas;
		}

		public GenerateConfig config { get; set; }
		public ClassConfig clas { get; set; }
	}
}
