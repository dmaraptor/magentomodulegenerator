﻿using MagentoModuleGenerator.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MagentoModuleGenerator.Templates.app.code.local.Namespace.Module.etc
{
    partial class config_xml
    {
        public config_xml(GenerateConfig config)
        {
            this.config = config;
        }

        public GenerateConfig config { get; set; }
    }
}
