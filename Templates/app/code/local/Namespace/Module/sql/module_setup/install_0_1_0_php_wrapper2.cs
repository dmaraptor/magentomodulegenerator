﻿using MagentoModuleGenerator.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MagentoModuleGenerator.Templates.app.code.local.Namespace.Module.sql.module_setup
{
	public partial class install_0_1_0_php_wrapper
	{
		public GenerateConfig config { get; set; }
		public string innerCode { get; set; }

		public install_0_1_0_php_wrapper(GenerateConfig config, string innerCode)
		{
			this.config = config;
			this.innerCode = innerCode;
		}
	}
}
