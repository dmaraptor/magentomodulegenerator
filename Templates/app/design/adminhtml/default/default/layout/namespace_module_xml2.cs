﻿using MagentoModuleGenerator.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MagentoModuleGenerator.Templates.app.design.adminhtml.default1.default1.layout
{
	public partial class namespace_module_xml
	{
		public namespace_module_xml(GenerateConfig config)
		{
			this.config = config;
		}

		public GenerateConfig config { get; set; }
	}
}
