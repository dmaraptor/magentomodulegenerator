﻿using MagentoModuleGenerator.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MagentoModuleGenerator.Templates.app.etc.modules
{
    partial class Namespace_Module_xml
    {
        public Namespace_Module_xml(GenerateConfig config)
        {
            this.config = config;
        }

        public GenerateConfig config { get; set; }
    }
}
