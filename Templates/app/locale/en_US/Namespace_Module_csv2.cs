﻿using MagentoModuleGenerator.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MagentoModuleGenerator.Templates.app.locale.en_US
{
	public partial class Namespace_Module_csv
	{
		public Namespace_Module_csv(GenerateConfig config)
		{
			this.config = config;
		}

		public GenerateConfig config { get; set; }
	}
}
